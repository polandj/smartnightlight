#
# Create the optimized resources to be uploaded 
# to the device byt outputing to the data directory.
# 
VERSION = 1.0
# Order matters for CSS and JS (can't just use find)
CSS = www/css/bootstrap.min.css www/css/bootstrap-theme.min.css
JS = www/js/jquery.min.js www/js/bootstrap.min.js www/js/vue.min.js www/js/vue-resource.min.js
FONTS = $(shell find www/fonts -type f -print)
IMAGES = $(shell find www/images -type f -print)

all: data/css/styles.css data/js/scripts.js data/fonts data/images data/favicon.ico data/index.html

deploy: all data/css/styles.css.gz data/js/scripts.js.gz

data/css/styles.css: ${CSS}
	mkdir -p data/css
	cat ${CSS} > data/css/styles.css

data/css/styles.css.gz: data/css/styles.css
	gzip -fr data/css/styles.css

data/js/scripts.js: ${JS}
	mkdir -p data/js
	cat ${JS} > data/js/scripts.js

data/js/scripts.js.gz: data/js/scripts.js
	gzip -fr data/js/scripts.js

data/fonts: ${FONTS}
	cp -r www/fonts data

data/images: ${IMAGES}
	cp -r www/images data

data/favicon.ico: www/favicon.ico
	cp www/favicon.ico data/favicon.ico

data/index.html: www/index.html
	cp www/index.html data/index.html

clean:
	rm -rf data

