/* ESP8266 SMART NIGHT LIGHT

   Changes LED color and behavior based on time of day

   Copyright(c) 2016, 2017 Jonathan Poland
*/
#include <EEPROM.h>
#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <TaskScheduler.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <Time.h>
#include <TimeLib.h>
#include <Timezone.h> //https://github.com/JChristensen/Timezone
#include <ArduinoOTA.h>

#include "FS.h"

#define INPUT_LDR A0
#define INPUT_BUTTON 4
#define LED_TINY_BLU 2
#define LED_GRN 12
#define LED_BLU 13
#define LED_RED 15
#define EEPROM_SIZE 512
#define MAX_EVENTS 20
#define ULONG_MAX 4294967295

#define INO_VERSION 6

// Different known behaviors for the LED
typedef enum {
  NONE,
  PULSE,
  BLINK,
  SOLID,
  ANY
} Action;
const char* b2s[] = {"NONE", "PULSE", "BLINK", "SOLID", "ANY"};

typedef struct rgb {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} RGB;

// What we'll be doing and when
typedef struct event {
  Action behavior;
  short hour;          // In military time [0-23]
  short minute;        // [0-59]
  RGB color;
} Event;

// What we write to and from EEPROM
typedef struct cfg {
  int offset;
  char name[20];
  Event events[MAX_EVENTS];
} Config;

// Globals
Ticker ticker;
WiFiUDP ntpUDP;
ESP8266WebServer server(80);
NTPClient timeClient(ntpUDP, "pool.ntp.org");
Scheduler runner;
WiFiManager wifiManager;
Config config;
Event* activeEvent = NULL;
bool web_reset = false;

// Timezone Globals
//US Eastern Time Zone (New York, Detroit)
TimeChangeRule usEDT = {"EDT", Second, Sun, Mar, 2, -240};  
TimeChangeRule usEST = {"EST", First, Sun, Nov, 2, -300};   
Timezone usET(usEDT, usEST);

//US Central Time Zone (Chicago, Houston)
TimeChangeRule usCDT = {"CDT", Second, dowSunday, Mar, 2, -300};
TimeChangeRule usCST = {"CST", First, dowSunday, Nov, 2, -360};
Timezone usCT(usCDT, usCST);

//US Mountain Time Zone (Denver, Salt Lake City)
TimeChangeRule usMDT = {"MDT", Second, dowSunday, Mar, 2, -360};
TimeChangeRule usMST = {"MST", First, dowSunday, Nov, 2, -420};
Timezone usMT(usMDT, usMST);

//Arizona is US Mountain Time Zone but does not use DST
Timezone usAZ(usMST, usMST);

//US Pacific Time Zone (Las Vegas, Los Angeles)
TimeChangeRule usPDT = {"PDT", Second, dowSunday, Mar, 2, -420};
TimeChangeRule usPST = {"PST", First, dowSunday, Nov, 2, -480};
Timezone usPT(usPDT, usPST);
// Pointer to current TZ
Timezone *tzp = NULL;

// Used during setup to blink the LED
void tick() {
  static int color = LED_BLU;
  static int brightness = 0;
  if (brightness == 0) {
    brightness = map(analogRead(INPUT_LDR), 0, 1024, 1, 255);
  } else {
    brightness = 0;
    if (color == LED_BLU) {
      color = LED_RED;
    } else if (color == LED_RED) {
      color = LED_GRN;
    } else if (color == LED_GRN) {
      color = LED_BLU;  
    }
  }
  if (color == LED_BLU) {
    analogWrite(LED_GRN, 0);
    analogWrite(LED_BLU, brightness);
  } else if (color == LED_RED) {
    analogWrite(LED_BLU, 0);
    analogWrite(LED_RED, brightness);  
  } else if (color == LED_GRN) {
    analogWrite(LED_RED, 0);
    analogWrite(LED_GRN, brightness);  
  }
}

// Be a Wifi access point if we don't have any wifi config
void configModeCallback (WiFiManager *wifiManagerPtr) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(wifiManagerPtr->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.1, tick);
}

void update_internal_offset(int offset) {
  if (offset == -14401) {
    offset = 0;
    tzp = &usET;
  } else if (offset == -18001) {
    offset = 0;
    tzp = &usCT;
  } else if (offset == -21601) {
    offset = 0;
    tzp = &usMT;
  } else if (offset == -25201) {
    offset = 0;
    tzp = &usPT;
  } else {
    tzp = NULL;
  }
  timeClient.setTimeOffset(offset);
}

// Set local clock via NTP
void time_init() {
  update_internal_offset(config.offset);
  timeClient.setUpdateInterval(900000);
  timeClient.begin();
  timeClient.update();
  Serial.print("Attempting to synchronize time..");
  while (ULONG_MAX - timeClient.getEpochTime() < abs(config.offset)) {
    timeClient.update();
    delay(1000);
    Serial.print(".");
  }
  if (tzp) {
    time_t tztime = tzp->toLocal(timeClient.getEpochTime());
    Serial.println(String(hour(tztime)) + ":" + minute(tztime));
  } else {
    Serial.println(timeClient.getFormattedTime());
  }
}

// Set up the network
void wifi_init() {
  wifiManager.setTimeout(300);
  wifiManager.setAPCallback(configModeCallback);
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    ESP.restart();
  }
}

void mdns_init() {
  const char * myname = strlen(config.name) ? config.name: "smartnightlight";
  if (!MDNS.begin(myname)) {
    Serial.println("Error setting my mDNS name");
  } else {
    Serial.println("mDNS started with name " + String(myname));
  }
  MDNS.addService("http", "tcp", 80);
}

void ota_init() {
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
    ESP.restart();
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("OTA updating enabled");
}

void config_default() {
  memset(&config, 0, sizeof(config));
  config.offset = -14401; // Default to EST
  // Default set of events
  // {behavior, hour, minute, Red, Green, Blue}
  config.events[0] = {BLINK, 7, 14, 0, 255, 0};
  config.events[1] = {PULSE, 7, 15, 0, 255, 0};
  config.events[2] = {BLINK, 12, 59, 255, 0, 0};
  config.events[3] = {PULSE, 13, 0, 255, 0, 0};
  config.events[4] = {PULSE, 14, 45, 0, 255, 0};
  config.events[5] = {PULSE, 19, 0, 0, 0, 255};
  config.events[6] = {BLINK, 19, 59, 255, 0, 0};
  config.events[7] = {PULSE, 20, 0, 255, 0, 0};
  sort(config.events, MAX_EVENTS);
}

// Load settings from EEPROM
void config_load() {
  uint32_t chipid;
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.get(0, chipid);
  if (chipid == ESP.getChipId()) {
    EEPROM.get(0 + sizeof(chipid), config);
    Serial.println("Loading saved configuration from EEPROM");
  } else {
    config_default();
  }
  EEPROM.end();
  sort(config.events, MAX_EVENTS);
}

// Save settings to EEPROM
void config_save() {
  uint32_t chipid = ESP.getChipId();
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.put(0, chipid);
  EEPROM.put(0 + sizeof(chipid), config);
  EEPROM.commit();
  EEPROM.end();
  Serial.println("Saved config of size " + String(sizeof(config) + sizeof(chipid)));
}

// Send all config as JSON
void sendAll()
{
  String json = "{";
  char buf[10];
  time_t tztime = tzp ? tzp->toLocal(timeClient.getEpochTime()) : timeClient.getEpochTime();

  json += "\"hour\": \"" + String(hour(tztime)) + "\",";
  json += "\"minute\": \"" + String(minute(tztime)) + "\",";
  json += "\"offset\": \"" + String(config.offset) + "\",";
  json += "\"name\": \"" + String(config.name) + "\",";
  json += "\"bversion\": \"" + String(INO_VERSION) + "\",";

  json += "\"events\":[";
  bool needComma = false;
  for (int i = MAX_EVENTS - 1; i >= 0; i--) {
    Event *ev = &config.events[i];
    if (!ev->behavior) {
      continue;
    }
    if (needComma) {
      json += ",";
    }
    json += "{";
    json += "\"hour\":" + String(ev->hour) + ",";
    json += "\"minute\":" + String(ev->minute) + ",";
    json += "\"behavior\":\"" + String(b2s[ev->behavior]) + "\",";
    sprintf(buf, "%02x%02x%02x", ev->color.r, ev->color.g, ev->color.b);
    json += "\"color\":\"" + String(buf) + "\",";
    json += "\"active\":" + String(ev == activeEvent ? "true" : "false");
    json += "}";
    needComma = true;
  }
  json += "]}";
  server.send(200, "text/json", json);
}

void httpError(String error) {
  server.send(400, "text/json", "{\"error\": \"" + error + "\"}");
}

void saveEvent()
{
  if (server.args() != 3 && server.args() != 5) {
    httpError("Incorrect number of arguments, must be three or five");
    return;
  }

  String edit_hour = server.arg("hour");
  String edit_minute = server.arg("minute");

  String hour_min = server.arg("time");
  if (hour_min.length() == 0) {
    httpError("Missing time argument");
    return;
  }
  int colonIndex = hour_min.indexOf(':');
  if (colonIndex == -1) {
    httpError("Invalid time format, missing colon");
    return;
  }
  int hour = hour_min.substring(0, colonIndex).toInt();
  int minute = hour_min.substring(colonIndex + 1).toInt();

  Event *edit_ev = NULL, *ev;
  if (edit_hour.length() && edit_minute.length()) {
    edit_ev = find_event(edit_hour.toInt(), edit_minute.toInt());
    if (!edit_ev) {
      httpError("Attempted to edit an invalid event");
      return;
    }
  }
  ev = find_event(hour, minute);
  if (edit_ev && ev) {
    // We're editing an event to collide with another existing event
    edit_ev->behavior = NONE;
  } else if (edit_ev && !ev) {
    // Simply editing an existing event
    ev = edit_ev;
  } else if (!edit_ev && !ev) {
    // Grab a "new" event off the end, if we can
    ev = &config.events[MAX_EVENTS - 1];
    if (ev->behavior != NONE) {
      for (int i = 0; i<MAX_EVENTS; i++) {
        Serial.println(format_event(&config.events[i]));
      }
      httpError("Too many events");
      return;
    }
  }
  ev->hour = hour;
  ev->minute = minute;

  String color = server.arg("color");
  if (color.length() != 7 || !color.startsWith("#")) {
    httpError("Missing or invalid color format");
    return;
  }
  ev->color.r = strtoul(color.substring(1, 3).c_str(), NULL, 16);
  ev->color.g = strtoul(color.substring(3, 5).c_str(), NULL, 16);
  ev->color.b = strtoul(color.substring(5, 7).c_str(), NULL, 16);

  ev->behavior = Action(server.arg("behavior").toInt());

  sort(config.events, MAX_EVENTS);
  config_save();
  event_check_cb();
  server.send(204, "text/json");
}

void saveOffset()
{
  static int oldOffset = 0;
  int newOffset = server.arg("offset").toInt();
  if (newOffset != config.offset) {
    config.offset = newOffset;
    config_save();
    update_internal_offset(newOffset);
    Serial.println("Offset: " + String(config.offset));
  }
  event_check_cb();
  server.send(204, "text/json");
}

void saveName()
{
  String newName = server.arg("name");
  if (newName.length() >= sizeof(config.name) - 1) {
    httpError("Name is too long");
    return;
  }
  if (newName != String(config.name)) {
    strncpy(config.name, newName.c_str(), sizeof(config.name));
    config_save();
    MDNS.setInstanceName(newName);
    Serial.println("Name: " + String(config.name));
  }
  server.send(204, "text/json");
}

void systemPost()
{
   String clearConfig = server.arg("clear_config");
   if (clearConfig == "on") {
    config_default();
    config_save();
   }
   String reset = server.arg("reboot");
   if (reset == "on") {
    web_reset = true;
   }
   server.send(204, "text/json");
}

// Setup handlers and start the web server
void webserver_init() {
  server.serveStatic("/", SPIFFS, "/index.html");
  server.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico", "max-age=86400");
  server.serveStatic("/fonts", SPIFFS, "/fonts", "max-age=86400");
  server.serveStatic("/js", SPIFFS, "/js", "max-age=86400");
  server.serveStatic("/css", SPIFFS, "/css", "max-age=86400");
  server.serveStatic("/images", SPIFFS, "/images", "max-age=86400");
  server.on("/r/all", HTTP_GET, sendAll);
  server.on("/r/event", HTTP_POST, saveEvent);
  server.on("/r/offset", HTTP_POST, saveOffset);
  server.on("/r/name", HTTP_POST, saveName);
  server.on("/r/system", HTTP_POST, systemPost);
  server.onNotFound([]() {
    String message = "File Not Found: " + server.uri() + "\n\n";
    message += "Be sure to populate the SPIFFS data!\n\n";
    message += "(firmware version " + String(INO_VERSION) + ")";
    server.send ( 404, "text/plain", message );
  });
  server.begin();
  Serial.println("Web Server started");
}

// Used to compare events for sorting by time.
// Note that a bigger time will come BEFORE a smaller time
int compare(const Event a, const Event b)
{
  if (a.behavior && b.behavior) {
    if (a.hour == b.hour) {
      if (a.minute == b.minute) {
        return 0;
      } else if (a.minute > b.minute) {
        return -1;
      } else {
        return 1;
      }
    } else if (a.hour > b.hour) {
      return -1;
    } else {
      return 1;
    }
  } else {
    if (a.behavior) {
      return -1;
    } else {
      return 1;
    }
  }
}

// Basic bubble sort
void sort(Event events[], int size) {
  for (int i = 0; i < (size - 1); i++) {
    for (int o = 0; o < (size - (i + 1)); o++) {
      if (compare(events[o], events[o + 1]) == 1) {
        Event t = events[o];
        events[o] = events[o + 1];
        events[o + 1] = t;
      }
    }
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GRN, OUTPUT);
  pinMode(LED_BLU, OUTPUT);
  pinMode(LED_TINY_BLU, OUTPUT);

  // Turn off tiny blu light
  digitalWrite(LED_TINY_BLU, HIGH);

  ticker.attach(0.5, tick);
  wifi_init();
  ticker.attach(0.5, tick);
  config_load();
  time_init();
  SPIFFS.begin();
  webserver_init();
  mdns_init();
  ota_init();
  ticker.detach();

  ESP.wdtEnable(5000);

  runner.startNow();
  Serial.println("Startup complete!");
}

void loop() {
  static int lastButtonPressStart = 0;
  MDNS.update();
  timeClient.update();
  server.handleClient();
  runner.execute();
  ArduinoOTA.handle();
  if (digitalRead(INPUT_BUTTON) == LOW) {
    // Button is pressed
    if (lastButtonPressStart == 0) {
      lastButtonPressStart = timeClient.getEpochTime();
    }
  } else if (lastButtonPressStart > 0) {
    // Button has been released
    int pressDuration = timeClient.getEpochTime() - lastButtonPressStart;
    if (pressDuration > 9) {
      Serial.println("Resetting CONFIG due to SUPER long button press");
      config_default();
      config_save();
    } else if (pressDuration > 4) {
      Serial.println("Resetting WIFI due to long button press");
      wifiManager.resetSettings();
    } else if (pressDuration > 1) {
      Serial.println("Resetting device due to short button press");
      ESP.restart();
    } else {
      Serial.println("Forcing time update due to button press");
      timeClient.forceUpdate();
    }
    lastButtonPressStart = 0;
  }
  if (web_reset) {
    Serial.println("Resetting device due to web request");
    ESP.restart();
  }
}

// Given an event, find the next event chronologically
// Skips disabled events and knows how to wrap the array
Event *next_event(Event * evp) {
  Event *minevp = &config.events[0];
  Event *maxevp = &config.events[MAX_EVENTS - 1];
  Event *nextevp = evp;
  do {
    if (nextevp - 1 < minevp) {
      nextevp = maxevp;
    } else {
      nextevp--;
    }
  } while (nextevp != evp && !nextevp->behavior);
  if (nextevp == evp) {
    nextevp = NULL;
  }
  return nextevp;
}

// Format the given event for the web or log
String format_event(Event * ev) {
  if (!ev) {
    return "None";
  }
  String rgb = String(ev->color.r) + ", " + String(ev->color.g) + ", " + String(ev->color.b);
  String retval = String(ev->hour) + "h" + String(ev->minute) + "m ";
  retval += "<span style=\"color: rgb(" + rgb + ")\">";
  if (ev->behavior == PULSE) {
    retval += "PULSE ";
  } else if (ev->behavior == BLINK) {
    retval += "BLINK ";
  }
  retval += "</span>";
  retval += "<span style=\"font-size: x-small; color: #999\">[" + rgb + "]</span>";
  return retval;
}

// Return event that covers current time
Event * find_active_event() {
  Event *ev, *retval = NULL, needle;
  time_t tztime = tzp ? tzp->toLocal(timeClient.getEpochTime()) : timeClient.getEpochTime();
 
  needle = {ANY, hour(tztime), minute(tztime), 0, 0, 0};
  for (int i = 0; i < MAX_EVENTS; i++) {
    ev = &config.events[i];
    if (!ev->behavior) {
      continue;
    }
    if (compare(needle, *ev) <= 0) {
      retval = ev;
      break;
    }
  }
  // Account for day rollover
  if (retval == NULL && config.events[0].behavior) {
    retval = &config.events[0];
  }
  return retval;
}

Event * find_event(int hour, int minute) {
  Event *ev, *retval = NULL;
  for (int i = 0; i < MAX_EVENTS; i++) {
    ev = &config.events[i];
    if (!ev->behavior) {
      continue;
    }
    if (ev->hour == hour && ev->minute == minute) {
      retval = ev;
      break;
    }
  }
  return retval;
}

// Pulse the LEDs per the current active event
void pulse_cb() {
  static float in = 0;
  in = in + 0.03;
  if (in > 6.283)
    in = 0;
  analogWrite(LED_RED, cos(in) * activeEvent->color.r / 2 + activeEvent->color.r / 2);
  analogWrite(LED_GRN, cos(in) * activeEvent->color.g / 2 + activeEvent->color.g / 2);
  analogWrite(LED_BLU, cos(in) * activeEvent->color.b / 2 + activeEvent->color.b / 2);
}

// Blink the LEDs per the current active event
void blink_cb() {
  static bool toggle = HIGH;
  if (toggle) {
    analogWrite(LED_RED, activeEvent->color.r);
    analogWrite(LED_GRN, activeEvent->color.g);
    analogWrite(LED_BLU, activeEvent->color.b);
  } else {
    analogWrite(LED_RED, 0);
    analogWrite(LED_GRN, 0);
    analogWrite(LED_BLU, 0);
  }
  toggle = !toggle;
}

Task periodic(1000, TASK_FOREVER, &event_check_cb, &runner, true);
Task pulser(50, TASK_FOREVER, &pulse_cb, &runner, false);
Task blinker(250, TASK_FOREVER, &blink_cb, &runner, false);

// Figure out if we need to transition events and if so set the LEDs
// as desired and setup the proper behavior events.
void event_check_cb() {
  Event * newActiveEvent = find_active_event();
  if (newActiveEvent != NULL && newActiveEvent != activeEvent) {
    analogWrite(LED_RED, newActiveEvent->color.r);
    analogWrite(LED_GRN, newActiveEvent->color.g);
    analogWrite(LED_BLU, newActiveEvent->color.b);
    activeEvent = newActiveEvent;
    if (activeEvent->behavior == PULSE) {
      blinker.disable();
      pulser.enable();
    } else if (activeEvent->behavior == BLINK) {
      pulser.disable();
      blinker.enable();
    } else if (activeEvent->behavior == SOLID) {
      pulser.disable();
      blinker.disable();
    }
  }
}


