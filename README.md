# ESP8266 Smart Night Light #

Turn the $5 Witty Cloud Development (ESP8266-12E) board into a programmable nightlight.

![witty.jpg](https://bitbucket.org/repo/6LKdbj/images/808783627-witty.jpg)

### Features

 * Easy setup and control via WiFi
 * HTML5 Responsive Web Interface works on all devices
 * Millions of light color options
 * Multiple light behaviors: pulse, blink, solid, off
 * Programmable name for easy identification
 * Configurable timezone
 * Up to twenty different saved events

### Uses

 * Teach young children that can't read a clock yet to get out of bed when their light is a certain color.
 * Give yourself a visual reminder when you should do or be doing something.

Checkout the [wiki](https://bitbucket.org/polandj/smartnightlight/wiki/Home) to get started!